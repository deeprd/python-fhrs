
Changelog
=========

0.2.0 (2019-01-16)
------------------

* Added documentation on ReadTheDocs


0.1.0 (2018-12-03)
------------------

* First release on PyPI.
